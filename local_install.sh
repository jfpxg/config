#!/bin/bash
SDIR=`dirname $(readlink -f "$0")`
CFGS="$SDIR/config"
CFGTARGET="$HOME"
SCRIPTS="$SDIR/scripts"
SCRIPTTARGET="/usr/local/bin"

if [ ! -f /etc/sudoers.d/$USER ]; then
    echo "$USER ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/$USER 
fi

if [ ! -d "$HOME/.oh-my-zsh" ]; then
    echo "Installing Oh My Zsh..."
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

IFS=$'\n'
_CFGS=(`ls -a $CFGS`)
_SCRIPTS=(`ls -a $SCRIPTS`)

echo "Working through configs..."
for _CFG in ${_CFGS[@]}; do
    [ -d $CFG/$_CFG ] && continue
    echo "  Linking $_CFG into $CFGTARGET"
    rm -f "$CFGTARGET/$_CFG" && \
        ln -s "$CFGS/$_CFG" "$CFGTARGET/$_CFG"
done

echo "Working through scripts..."
for _SCR in ${_SCRIPTS[@]}; do
    [ -d $SCRIPTS/$_SCR ] && continue
    echo "  Copying $_SCR into $SCRIPTTARGET"
    sudo rm -f "$SCRIPTTARGET/$_SCR" && \
        sudo cp "$SCRIPTS/$_SCR" "$SCRIPTTARGET/$_SCR"
done

